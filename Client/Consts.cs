﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client
{
    public class Consts
    {
        public class Commands
        {
            public const string Login = "login";
            public const string Register = "register";
            public const string Lobby = "lobby";
            public const string Error = "error";
            public const string Matchmaking = "matchmaking";
            public const string MatchmakingCancel = "matchmakingCancel";
            public const string MatchFound = "matchFound";
            public const string MatchStart = "matchStart";
        }
    }
}

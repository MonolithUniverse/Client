﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Net;
using System.Net.Sockets;

namespace Client
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private void Application_Startup(object sender, StartupEventArgs e)
        {
            ServerConnection.Instance.Connect();
            if (!ServerConnection.Instance.Connected)
            {
                MessageBox.Show("Connection with server isn't established");
            }
            Views.LoginWindow loginWindow = new Views.LoginWindow();
            loginWindow.Show();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Client.Views
{
    /// <summary>
    /// Interaction logic for LoginWindow.xaml
    /// </summary>
    public partial class LoginWindow : Window
    {
        ViewModel.LoginViewModel viewModel;
        public LoginWindow()
        {
            InitializeComponent();
            viewModel = new ViewModel.LoginViewModel();
            this.loginGrid.DataContext = viewModel.User;
            this.logInButton.DataContext = viewModel;
            this.signInButton.DataContext = viewModel;
            this.changeSettingsButton.TextDecorations = null;
            this.changeSettingsButton.DataContext = viewModel;
        }

        private void PasswordEditor_passwordChanged(object sender, RoutedEventArgs e)
        {
            if (viewModel != null)
            {
                // just for tests changed type from secureString to string
                viewModel.Password = ((PasswordBox)sender).Password;
            }
        }
    }
}

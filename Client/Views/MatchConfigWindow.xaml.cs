﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Client.Views
{
    /// <summary>
    /// Interaction logic for MatchConfigWindow.xaml
    /// </summary>
    public partial class MatchConfigWindow : Window
    {
        public MatchConfigWindow(Model.Game game)
        {
            InitializeComponent();
            var characters = game.GetCharacters();
            this.matchConfogWindow.DataContext = game;
            this.characterInfo1.DataContext = characters[0];
            this.characterRadioButton1.DataContext = characters[0];
            viewModel = new ViewModel.MatchConfigViewModel(game);
            this.matchmakingStatus.DataContext = viewModel;
            viewModel.CloseAction = new Action(this.Close);
        }

        private ViewModel.MatchConfigViewModel viewModel;
    }
}

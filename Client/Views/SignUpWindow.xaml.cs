﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Client.Views
{
    /// <summary>
    /// Interaction logic for SignInWindow.xaml
    /// </summary>
    public partial class SignUpWindow : Window
    {
        public SignUpWindow()
        {
            InitializeComponent();
            viewModel = new ViewModel.SignUpViewModel();
            this.signInGrid.DataContext = viewModel.User;
            this.signUpButton.DataContext = viewModel;
            this.signUpButton.DataContext = viewModel;
            this.changeSettingsButton.DataContext = viewModel;
            this.changeSettingsButton.TextDecorations = null;
            viewModel.CloseAction = new Action(this.Close);
        }

        private void PasswordEditor_passwordChanged(object sender, RoutedEventArgs e)
        {
            if (viewModel != null)
            {
                // just for tests changed type from secureString to string
                viewModel.Password = ((PasswordBox)sender).Password;
                if(this.passwordEditor.Password.Length == 0)
                {
                    this.passwordConfirmationError.Text = "";
                }
            }
        }

        private void PasswordConfirmation_passwordChanged(object sender, RoutedEventArgs e)
        {
            if (viewModel != null)
            {
                // just for tests changed type from secureString to string
                viewModel.PasswordConfirmation = ((PasswordBox)sender).Password;
                if(this.passwordEditor.Password.Equals(this.passwordConfirmation.Password))
                {
                    this.passwordConfirmationError.Foreground = new SolidColorBrush(Colors.GreenYellow);
                    this.passwordConfirmationError.Text = "Confirmed";
                }
                else
                {
                    this.passwordConfirmationError.Foreground = new SolidColorBrush(Colors.Red);
                    this.passwordConfirmationError.Text = "Not match";
                }
            }
        }

        private ViewModel.SignUpViewModel viewModel;
    }
}

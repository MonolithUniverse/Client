﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client.ContentModels
{
    public class MatchFoundContent
    {
        public string[] Allies
        {
            get;
            set;
        }

        public string Team
        {
            get;
            set;
        }
    }
}

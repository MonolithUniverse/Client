﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client.ContentModels
{
    class RegisterContent : Content
    {
        public string Login { get; set; }
        public string Password { get; set; }
        public string Username
        { get; set; }
    }
}

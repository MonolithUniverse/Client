﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client.ContentModels
{
    public class LoginContent : Content
    {
        public string Login { get; set; }
        public string Password { get; set; }
    }
}

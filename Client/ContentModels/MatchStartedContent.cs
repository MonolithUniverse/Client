﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client.ContentModels
{
    class MatchStartedContent : Content
    {
        public int Port { set; get; }
        public string Team { get; set; }
    }
}

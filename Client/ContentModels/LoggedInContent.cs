﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client.ContentModels
{
    class LoggedInContent : Content
    {
        public string Username { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Timers;

namespace Client.ViewModel
{
    class MainWindowViewModel : INotifyPropertyChanged
    {
        public MainWindowViewModel(Model.UserModel user)
        {
            User = user;
            Game = new Model.Game(user);
            PlayCommand = new MyICommand(Play);
            MatchmakingStatus = matchmakingStatus;
            MatchmakingStatus = "Click Play button to find match";
            Game.PropertyChanged += Game_PropertyChanged;
            timer.Elapsed += Timer_Elapsed;
        }

        public string MatchmakingStatus
        {
            get
            {
                return matchmakingStatus;
            }
            set
            {
                matchmakingStatus = value;
                RaisePropertyChanged("MatchmakingStatus");
            }
        }

        public  MyICommand PlayCommand
        {
            get;
            set;
        }

        public Model.UserModel User { get; set; }
        public Model.Game Game { get; set; }
      
        public event PropertyChangedEventHandler PropertyChanged;
        private void RaisePropertyChanged(string property)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(property));
            }
        }

        private void Timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            timeToStartMatch--;
            MatchmakingStatus = "Time to start match: " + timeToStartMatch + "seconds";
            if (timeToStartMatch == 0)
            {
                timer.Stop();
                MatchmakingStatus = "Click Play button to find match";
            }
        }

        private void Game_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Found" && Game.Found)
            {
                Client.Views.MatchConfigWindow matchConfigWindow = new Views.MatchConfigWindow(Game);
                matchConfigWindow.Show();
                MatchmakingStatus = "Match has been found";
            }
            else if (e.PropertyName == "ServerStarted" && Game.ServerStarted)
            {
                timeToStartMatch = 10;
                timer.Start();
                MatchmakingStatus = "Time to start match: " + timeToStartMatch + "seconds";
            }
            else if (e.PropertyName == "MatchmakingOn" && Game.MatchmakingOn)
            {
                MatchmakingStatus = "We are seacrhing match for You...";
            }
        }

        private void Play()
        {
            if (!ServerConnection.Instance.Connected)
            {
                MatchmakingStatus = "Connection with server is not established";
                return;
            }
            if (!Game.GameStarted)
            {
                Game.Search();
            }
            else
            {
                MatchmakingStatus = "Close current match and try again";
            }
        }

        private string matchmakingStatus;
        private System.Timers.Timer timer = new System.Timers.Timer(1000);
        private int timeToStartMatch = -1;
    }
}

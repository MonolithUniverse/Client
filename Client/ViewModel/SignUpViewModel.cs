﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;

namespace Client.ViewModel
{
    class SignUpViewModel
    {
        public SignUpViewModel()
        {
            User = new Model.UserModel();
            User.PropertyChanged += User_PropertyChanged;
            SignUpCommand = new MyICommand(SignUp);
            ChangeSettingsCommand = new MyICommand(ChangeSettings);
        }

        public MyICommand SignUpCommand
        {
            get;
            set;
        }
        public MyICommand ChangeSettingsCommand
        {
            get;
            set;
        }

        public Model.UserModel User
        {
            get;
            set;
        }

        public String Password
        {
            private get;
            set;
        }

        public String PasswordConfirmation
        {
            private get;
            set;
        }
        public Action CloseAction
        {
            get;
            set;
        }

        private void User_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Authorized")
            {
                if (CloseAction != null)
                    CloseAction();
            }
        }

        private void ChangeSettings()
        {
            Views.SettingsWindow settingsWindow = new Views.SettingsWindow();
            settingsWindow.Show();
        }

        private void SignUp()
        {
            if (Password != null &&
                PasswordConfirmation != null &&
                Password.Equals(PasswordConfirmation))
            {
                User.Register(Password);
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;

namespace Client.ViewModel
{
    public class LoginViewModel
    {
        public LoginViewModel()
        {
            User = new Model.UserModel();
            User.PropertyChanged += User_PropertyChanged;
            LogInCommand = new MyICommand(LogIn, CanLogIn);
            SignInCommand = new MyICommand(SignIn, CanSignIn);
            ChangeSettingsCommand = new MyICommand(ChangeSettings, CanChangeSettings);
        }

        public MyICommand LogInCommand
        {
            get;
            set;
        }
        public MyICommand SignInCommand
        {
            get;
            set;
        }
        public MyICommand ChangeSettingsCommand
        {
            get;
            set;
        }

        public Model.UserModel User
        {
            get;
            set;
        }

        // just for tests changed type from SecureString to String for password
        public String Password
        {
            private get;
            set;
        }

        private void User_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Authorized")
            {
                MainWindow mainWindow = new MainWindow(User);
                App.Current.MainWindow.Close();
                App.Current.MainWindow = mainWindow;
                mainWindow.Show();
            }
        }

        private void LogIn()
        {
            User.Authorize(Password);
            /* {

             }*/
        }

        private bool CanLogIn()
        {
            return true;
        }

        private void SignIn()
        {
            Views.SignUpWindow signInWindow = new Views.SignUpWindow();
            signInWindow.ShowDialog();
        }

        private bool CanSignIn()
        {
            return true;
        }

        private void ChangeSettings()
        {
            Views.SettingsWindow settingsWindow = new Views.SettingsWindow();
            settingsWindow.Show();
        }

        private bool CanChangeSettings()
        {
            return true;
        }
    }
}

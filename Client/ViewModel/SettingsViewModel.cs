﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Client.ViewModel
{
    class SettingsViewModel
    {
        public SettingsViewModel()
        {
            ServerAddress = Properties.Settings.Default.serverAddress;
            ServerPort = Properties.Settings.Default.serverPort;
            SaveCommand = new MyICommand(SaveSettings);
            CancelCommand = new MyICommand(CancelAction);
        }

        public string ServerAddress
        {
            get;
            set;
        }
        public int ServerPort
        {
            get;
            set;
        }

        public MyICommand SaveCommand
        {
            get;
            set;
        }
        public MyICommand CancelCommand
        {
            get;
            set;
        }

        public Action CloseAction
        {
            get;
            set;
        }

        private void SaveSettings()
        {
            Properties.Settings.Default.serverAddress = ServerAddress;
            Properties.Settings.Default.serverPort = ServerPort;
            Properties.Settings.Default.Save();
            ServerConnection.Instance.Connect();
            if (!ServerConnection.Instance.Connected)
            {
                MessageBox.Show("Connection with server isn't established");
            }
            if (CloseAction != null)
                CloseAction();
        }

        private void CancelAction()
        {
            if (CloseAction != null)
                CloseAction();
        }
    }
}

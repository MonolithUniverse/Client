﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using static System.Net.Mime.MediaTypeNames;

namespace Client.ViewModel
{
    public class MatchConfigViewModel : INotifyPropertyChanged
    {
        public MatchConfigViewModel(Model.Game game)
        {
            this.game = game;
            game.PropertyChanged += Game_PropertyChanged;
            timer.Elapsed += Timer_Elapsed;
            CloseActionCommand = new MyICommand(CloseActionExecute);
        }


        public MyICommand CloseActionCommand
        {
            get;
            set;
        }

        public string MatchmakingStatus
        {
            get
            {
                return matchmakingStatus;
            }
            set
            {
                matchmakingStatus = value;
                RaisePropertyChanged("MatchmakingStatus");
            }
        }

        public Action CloseAction
        {
            get;
            set;
        }

        private void Game_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "ServerStarted" && game.ServerStarted)
            {
                timeToStartMatch = 10;
                timer.Start();
                MatchmakingStatus = "Time to start match: " + timeToStartMatch + "seconds";
            }
            else if (e.PropertyName == "GameStarted" && game.GameStarted)
            {
                App.Current.Dispatcher.Invoke(CloseAction);
            }
        }

        private void CloseActionExecute()
        {
            if (CloseAction != null)
                CloseAction();
        }
        private void Timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            timeToStartMatch--;
            MatchmakingStatus = "Time to start match: " + timeToStartMatch + "seconds";
            if (timeToStartMatch == 0)
            {
                timer.Stop();
                MatchmakingStatus = "";
                if (!game.GameStarted)
                    game.start();
            }
        }

        private void RaisePropertyChanged(string property)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(property));
            }
        }

        private System.Timers.Timer timer = new System.Timers.Timer(1000);
        private int timeToStartMatch = -1;
        private Model.Game game;
        private string matchmakingStatus;

        public event PropertyChangedEventHandler PropertyChanged;
    }
}

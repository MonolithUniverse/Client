﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client.Model
{
    public class Game : INotifyPropertyChanged
    {

        public Game(UserModel user)
        {
            User = user;
            possibleCharacters = new List<ICharacter>();
            possibleCharacters.Add(new Soldier());
            possibleCharacters.Last().IsChecked = true;
            InstanceID = Guid.NewGuid();
            Console.WriteLine("game :" + InstanceID);
        }

        public bool MatchmakingOn
        {
            get
            {
                return matchmakingOn;
            }

            set
            {
                if(!matchmakingOn.Equals(value))
                {
                    matchmakingOn = value;
                    RaisePropertyChanged("MatchmakingOn");
                }
            }
        }

        public bool Found
        {
            get
            {
                return found;
            }
            set
            {
                if(!found.Equals(value))
                {
                    found = value;
                    RaisePropertyChanged("Found");
                }
            }
        }

        public UserModel User
        {
            get;
            set;
        }

        public bool ServerStarted
        {
            get
            {
                return serverStarted;
            }
            set
            {
                if(!serverStarted.Equals(value))
                {
                    serverStarted = value;
                    RaisePropertyChanged("ServerStarted");
                }
            }
        }

        public bool GameStarted
        {
            get
            {
                return gameStarted;
            }
            set
            {
                if (!gameStarted.Equals(value))
                {
                    gameStarted = value;
                    RaisePropertyChanged("GameStarted");
                }
            }
        }

        public string Teammate
        {
            get
            {
                return teamMate;
            }
            set
            {
                if(!teamMate.Equals(value))
                {
                    teamMate = value;
                    RaisePropertyChanged("Teammate");
                }
            }
        }

        public int Port
        {
            get
            {
                return port;
            }
            set
            {
                if (!port.Equals(value))
                {
                    port = value;
                    RaisePropertyChanged("Port");
                }
            }
        }

        public string Team
        {
            get
            {
                return team;
            }
            set
            {
                if(!team.Equals(value))
                {
                    team = value;
                    RaisePropertyChanged("Team");
                }
            }
        }

        public Guid InstanceID { get; private set; }

        public string Character
        {
            get
            {
                foreach (var character in possibleCharacters)
                {
                    if (character.IsChecked)
                        return character.Name;
                }
                return "";
            }
        }

        public void Search()
        {
            ServerConnection.Instance.FindMatch(this);
        }

        public void start()
        {
            lock (this)
            {
                if (!GameStarted)
                {
                    GameStarted = true;
                    Console.WriteLine("Starting game by " + InstanceID);
                    Process game = new Process();
                    game.StartInfo.WorkingDirectory = "game";
                    game.StartInfo.FileName = "MonolithUniverse.exe";
                    game.StartInfo.WindowStyle = ProcessWindowStyle.Maximized;
                    Console.WriteLine("port: " + port + Properties.Settings.Default.serverAddress);
                    int teamNumber = team.Equals("blue") ? 1 : 0;
                    game.StartInfo.Arguments = $"{Properties.Settings.Default.serverAddress}:{port}?team={teamNumber}";
                    game.EnableRaisingEvents = true;
                    game.Exited += Game_Exited;
                    game.Start();
                }
            }
        }

        private void Game_Exited(object sender, EventArgs e)
        {
            GameStarted = false;
            MatchmakingOn = false;
            ServerStarted = false;
            Found = false;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void RaisePropertyChanged(string property)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(property));
            }
        }

        public List<ICharacter>  GetCharacters()
        {
            return possibleCharacters;
        }

        private List<ICharacter> possibleCharacters;
        private string team = "";
        private string teamMate = "";
        private bool found = false;
        private bool serverStarted = false;
        private int port;
        private bool matchmakingOn = false;
        private bool gameStarted = false;
    }
}

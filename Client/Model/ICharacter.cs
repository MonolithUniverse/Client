﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client.Model
{
    public class ICharacter
    {
        virtual public string Name
        {
            get;
        }

        virtual public string SpecialSkill
        {
            get;
        }

        virtual public string SpecialAttack
        {
            get;
        }

        virtual public string   BasicAttack
        {
            get;
        }

        public bool IsChecked
        {
            get;
            set;
        }
    }
}

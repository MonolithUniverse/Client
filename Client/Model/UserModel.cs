﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Security;
using Client;

namespace Client.Model
{
    public class UserModel : INotifyPropertyChanged
    {
        public string Login
        {
            get
            {
                return this.login;
            }
            set
            {
                if(login != value)
                {
                    login = value;
                    RaisePropertyChanged("Login");
                }
            }
        }

        public string Username
        {
            get
            {
                return this.username;
            }
            set
            {
                if(username != value)
                {
                    username = value;
                    RaisePropertyChanged("Username");
                }
            }
        }

        public bool Authorized
        {
            get
            {
                return authorized;
            }
            set
            {
                if(authorized != value)
                {
                    authorized = value;
                    RaisePropertyChanged("Authorized");
                }
            }
        }

        public string LastError
        {
            get
            {
                return lastError;
            }
            set
            {
                if(!lastError.Equals(value))
                {
                    lastError = value;
                    RaisePropertyChanged("LastError");
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void RaisePropertyChanged(string property)
        {
            if(PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(property));
            }
        }

        public void Authorize(String password)
        {
            if (!ServerConnection.Instance.Connected)
            {
                LastError = "Connection problem";
                return;
            }
            ServerConnection.Instance.LogIn(this, password);
        }

        public void Register(String password)
        {
            if (!ServerConnection.Instance.Connected)
            {
                LastError = "Connection problem";
                return;
            }
            ServerConnection.Instance.Register(this, password);
        }

        private string login = "";
        private string username = "";
        private bool authorized = false;
        private string lastError = "";
    }
}

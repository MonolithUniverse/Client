﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client.Model
{
    public class Soldier : ICharacter
    {
        override public string Name
        {
            get
            {
                return "Soldier";
            }
        }

        override public string SpecialSkill
        {
            get
            {
                return "extra speed - speeds up for short time";
            }
        }

        override public string SpecialAttack
        {
            get
            {
                return "triple shot - releases three bullets in short period of time";
            }
        }

        override public string BasicAttack
        {
            get
            {
                return "simple shot with average speed and damage";
            }
        }
    }
}

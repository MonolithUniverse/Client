﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Client
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow(Model.UserModel account)
        {
            InitializeComponent();
            viewModel = new ViewModel.MainWindowViewModel(account);
            this.userInfoText.DataContext = viewModel.User;
            this.mainWindow.DataContext = viewModel;
            this.matchmakingStatus.DataContext = viewModel;
        }

        ViewModel.MainWindowViewModel viewModel;
    }
}

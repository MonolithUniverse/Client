﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Client
{
    public class ServerConnection
    {
        public bool Connect()
        {
            try
            {
                client = new TcpClient(Properties.Settings.Default.serverAddress, Properties.Settings.Default.serverPort);
                sr = new StreamReader(Stream);
                startReceiving();
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return false;
            }
        }

        public bool Connected
        {
            get
            {
                if (client == null)
                    return false;
                return client.Connected;
            }
        }

        public void Stop()
        {
            stop = true;
        }

        public async void startReceiving()
        {
            stop = false;
            while(!stop && ServerConnection.Instance.Connected)
            {
                try
                {
                    var msg = await ReceiveMsgAsync();
                    Console.WriteLine(msg);
                    var jObject = JObject.Parse(msg);
                    var jCommand = jObject["command"];
                    if (jCommand.ToString().Equals(Consts.Commands.Register))
                    {
                        currentUser.Authorized = true;
                    }
                    else if (jCommand.ToString().Equals(Consts.Commands.Login))
                    {
                        currentUser.Username = jObject["content"].ToObject<ContentModels.LoggedInContent>().Username;
                        currentUser.Authorized = true;
                    }
                    else if (jCommand.ToString().Equals(Consts.Commands.Error))
                    {
                        currentUser.LastError = (jObject["content"].ToObject<ContentModels.ErrorContent>().Message);
                    }
                    else if (jCommand.ToString().Equals(Consts.Commands.MatchFound))
                    {
                        if (game != null)
                        {
                            ContentModels.MatchFoundContent content = jObject["content"].ToObject<ContentModels.MatchFoundContent>();
                            if (game != null)
                            {
                                game.Team = content.Team;
                                foreach (string allie in content.Allies)
                                {
                                    if (!game.User.Username.Equals(allie))
                                        game.Teammate = allie;
                                }
                            }

                            game.Found = true;
                        }
                    }
                    else if (jCommand.ToString().Equals(Consts.Commands.MatchStart))
                    {
                        if (game != null)
                        {
                            game.Port = jObject["content"].ToObject<ContentModels.MatchStartedContent>().Port;
                            game.ServerStarted = true;
                        }
                    }
                    else if (jCommand.ToString().Equals(Consts.Commands.Matchmaking))
                    {
                        if (game != null)
                        {
                            game.MatchmakingOn = true;
                        }
                    }
                    else
                    {
                        Console.WriteLine(jObject.ToString());
                    }
                }
                catch(Exception)
                {

                }
            }
        }

        public void Register(Model.UserModel user, string password)
        {
            this.currentUser = user;
            if (!ServerConnection.Instance.Connected)
                return;
            Sender sender = new Sender(Instance.Stream);
            ContentModels.RegisterContent content = new ContentModels.RegisterContent { Login = user.Login, Password = password, Username = user.Username };
            sender.SendMessageAsync(MessageCreator.CreateCommandContentMsg<ContentModels.RegisterContent>(Consts.Commands.Register, content));
        }

        public void FindMatch(Model.Game game)
        {
            this.game = game;
            if (!ServerConnection.Instance.Connected)
                return;
            Sender sender = new Sender(Instance.Stream);
            sender.SendMessageAsync(MessageCreator.CreateCommandContentMsg(Consts.Commands.Matchmaking));
        }

        public void LogIn(Model.UserModel user, string password)
        {
            this.currentUser = user;
            if (!ServerConnection.Instance.Connected)
                return;
            Sender sender = new Sender(Instance.Stream);
            ContentModels.LoginContent content = new ContentModels.LoginContent { Login = user.Login, Password = password};
            sender.SendMessageAsync(MessageCreator.CreateCommandContentMsg<ContentModels.LoginContent>(Consts.Commands.Login, content));
        }

        public async Task<string> ReceiveMsgAsync()
        {
            return await this.sr.ReadLineAsync();
        }

        public NetworkStream Stream
        {
            get
            {
                if(client != null)
                {
                    return client.GetStream();
                }
                return null;
            }
        }

        public static ServerConnection Instance
        {
            get
            {
                if (instance == null)
                    instance = new ServerConnection();
                return instance;
            }
        }

        public Model.UserModel User { get; }

        public class Sender
        {
            private readonly StreamWriter sw;

            public Sender(Stream stream)
            {
                this.sw = new StreamWriter(stream) { AutoFlush = true };
            }

            public void SendMessageAsync(string msg)
            {
                sw.WriteLineAsync(msg);
            }
        }

        // will be encapsulated
        private Model.UserModel currentUser;
        private Model.Game game;

        private TcpClient client;
        private static ServerConnection instance = null;
        private bool stop = true;
        private StreamReader sr;
    }
}

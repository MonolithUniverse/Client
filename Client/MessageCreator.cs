﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client
{
    public class MessageCreator
    {
        public static string CreateCommandContentMsg(string command)
        {
            var message = new { command = command };
            return JsonConvert.SerializeObject(message);
        }

        public static string CreateCommandContentMsg<T>(string command, T content) where T : class
        {
            var message = new { command = command, content = content };
            return JsonConvert.SerializeObject(message);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client
{
    public class Message<T> where T : Content
    {
        public string Command { get; set; }
        public T Content { get; set; }
    }
}
